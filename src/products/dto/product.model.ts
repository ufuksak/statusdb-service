export class ProductDto{

    constructor(public id: number, public title: string, public description: string, public price: number, unit: string) {
    }
}
